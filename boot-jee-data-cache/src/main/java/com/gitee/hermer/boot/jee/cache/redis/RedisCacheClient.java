package com.gitee.hermer.boot.jee.cache.redis;

import java.util.List;

import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.RedisTemplate;

import com.gitee.hermer.boot.jee.cache.Cache;
import com.gitee.hermer.boot.jee.cache.CacheListener;
import com.gitee.hermer.boot.jee.cache.properties.BootCacheProperties;
import com.gitee.hermer.boot.jee.commons.collection.CollectionUtils;
import com.gitee.hermer.boot.jee.commons.collection.StringCache;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;

public class RedisCacheClient implements Cache{


	private RedisTemplate<String,Object> redisTemplate;

	private BootCacheProperties properties;

	private String regionName;

	private CacheListener listener;

	public RedisCacheClient(String regionName, RedisTemplate client,BootCacheProperties properties,CacheListener listener) {
		this.listener = listener;
		this.regionName = new StringCache(StringUtils.defaultIfEmpty(properties.getRedisNamespace(),
				"boot-cache")).append(":").append(regionName).toString();
		this.redisTemplate = client;
		this.properties = properties;
	}


	public String getRegion(Object key){
		if(key != null && StringUtils.isNotBlank(key.toString()) && key.toString().indexOf(regionName) != 0)
			return new StringCache(regionName).append("-").append(key).toString();
		return (String)key;
	}
	


	@Override
	public Object get(Object key) throws com.gitee.hermer.boot.jee.cache.exception.CacheException {
		return redisTemplate.opsForValue().get(getRegion(key));
	}

	@Override
	public void put(Object key, Object value) throws com.gitee.hermer.boot.jee.cache.exception.CacheException {
		redisTemplate.opsForValue().set(getRegion(key), value);

	}

	@Override
	public void put(Object key, Object value, Integer expireInSec)
			throws com.gitee.hermer.boot.jee.cache.exception.CacheException {
		redisTemplate.opsForValue().set(getRegion(key), value, expireInSec, TimeUnit.SECONDS);

	}

	@Override
	public void update(Object key, Object value) throws com.gitee.hermer.boot.jee.cache.exception.CacheException {
		put(getRegion(key),value);
	}

	@Override
	public void update(Object key, Object value, Integer expireInSec)
			throws com.gitee.hermer.boot.jee.cache.exception.CacheException {
		put(getRegion(key), value, expireInSec);

	}

	@Override
	public List keys() throws com.gitee.hermer.boot.jee.cache.exception.CacheException {
		return CollectionUtils.newArrayList(redisTemplate.keys(regionName+"*")); 
	}

	@Override
	public void evict(Object key) throws com.gitee.hermer.boot.jee.cache.exception.CacheException {
		redisTemplate.delete(getRegion(key));
	}

	@Override
	public void evict(List keys) throws com.gitee.hermer.boot.jee.cache.exception.CacheException {
		for (Object key : keys) {
			if(StringUtils.isNotEmpty((String)key))
				evict(getRegion(key));
		}

	}

	@Override
	public void clear() throws com.gitee.hermer.boot.jee.cache.exception.CacheException {
		evict(keys());
	}

	@Override
	public void destroy() throws com.gitee.hermer.boot.jee.cache.exception.CacheException {

	}
	
	



}

package com.gitee.hermer.boot.jee.cache.exception;

public class CacheException extends Exception{
	
	public CacheException(Throwable e){
		super(e);
	}
	
	public CacheException(String msg){
		super(msg);
	}

}

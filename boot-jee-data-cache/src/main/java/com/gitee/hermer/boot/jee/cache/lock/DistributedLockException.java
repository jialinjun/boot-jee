package com.gitee.hermer.boot.jee.cache.lock;

import com.gitee.hermer.boot.jee.commons.exception.PaiUException;

public class DistributedLockException extends PaiUException{

	public DistributedLockException(IErrorCode code) {
		super(code);
	}

	public DistributedLockException(IErrorCode code, String... formatArgs) {
		super(code,formatArgs);

	}
}

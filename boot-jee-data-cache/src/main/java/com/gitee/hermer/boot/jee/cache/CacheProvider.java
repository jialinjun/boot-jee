package com.gitee.hermer.boot.jee.cache;

import com.gitee.hermer.boot.jee.cache.exception.CacheException;
import com.gitee.hermer.boot.jee.cache.properties.BootCacheProperties;

public interface CacheProvider {

	public String name();

	public Cache buildCache(String regionName, boolean autoCreate,CacheListener listener) throws CacheException;


	public  void start(BootCacheProperties properties) throws CacheException;


	public void stop();

}

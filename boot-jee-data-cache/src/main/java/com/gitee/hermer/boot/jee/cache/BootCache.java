package com.gitee.hermer.boot.jee.cache;

import java.util.List;

import com.gitee.hermer.boot.jee.commons.bean.utils.BeanLocator;
import com.gitee.hermer.boot.jee.commons.verify.Assert;

public class BootCache {

	private  BootCache() {

	}
	private static CacheChannel CACHE_SERVICE;
	
	static{
		try{
			CACHE_SERVICE = BeanLocator.getBean(BootCacheChannel.class);
		}catch (Exception e) {
		}
	}


	/**
	 * 
	 * @Description: 将对象缓存
	 * @param id 目录标识
	 * @param key
	 * @param val 
	 * @return void  
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:36:32
	 */
	public static  void save(String id,Object key,Object val)
	{
		if(CACHE_SERVICE == null)
			return;
			
		Assert.hasText(id);
		Assert.notNull(key);
		CACHE_SERVICE.set(id,key,val);
	}

	/**
	 * 
	 * @Description: 缓存中取对象
	 * @param id 目录标识
	 * @param key 
	 * @return Object  
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:05:02
	 */
	public static <T> T get(String id,Object key)
	{
		if(CACHE_SERVICE == null)
			return null;
		Assert.hasText(id);
		Assert.notNull(key);
		return CACHE_SERVICE.get(id,key);
	}
	/**
	 * 
	 * @Description: 缓存删除对象
	 * @param id 目录标识
	 * @param key    
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:05:40
	 */
	public static void remove(String id,Object key)
	{
		if(CACHE_SERVICE == null)
			return;
		Assert.hasText(id);
		Assert.notNull(key);
		CACHE_SERVICE.evict(id,key);
	}

	public static Integer ping(){
		Assert.notNull(CACHE_SERVICE);
		CACHE_SERVICE.set("test", 1, 1);
		return CACHE_SERVICE.get("test", 1);
	}

	/**
	 * 
	 * @Description: 获取该目录下所有key
	 * @param id 目录标识
	 * @return List 
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:38:20
	 */
	public static List keys(String id)
	{
		if(CACHE_SERVICE == null)
			return null;
		Assert.hasText(id);
		try{
			return CACHE_SERVICE.keys(id);
		}catch (Exception e) {
			return null;
		}
	}
	/**
	 * 
	 * @Description: 获取该目录的大小
	 * @param id 目录标识
	 * @return  
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:39:01
	 */
	public static int size(String id)
	{
		if(CACHE_SERVICE == null)
			return 0;
		Assert.hasText(id);
		try{
		if(id != null && CACHE_SERVICE.keys(id)!=null)
			return CACHE_SERVICE.keys(id).size();
		else
			return 0;
		}catch (Exception e) {
			return 0;
		}
	}
	/**
	 * 
	 * @Description: 移除该目录所有数据
	 * @param id  目录标识
	 * @throws
	 * @author tumc
	 * @date 2016-3-10 下午1:40:16
	 */
	public static void clear(String id)
	{
		if(CACHE_SERVICE == null)
			return;
		Assert.hasText(id);
		if(size(id) > 0)
			CACHE_SERVICE.clear(id);
	}



}

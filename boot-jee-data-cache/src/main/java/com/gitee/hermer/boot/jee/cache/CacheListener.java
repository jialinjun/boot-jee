package com.gitee.hermer.boot.jee.cache;

public interface CacheListener {
	
	public void notifyCacheExpired(String region, Object key);

}

package com.gitee.hermer.boot.jee.generate.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import com.gitee.hermer.boot.jee.commons.utils.StringUtils;

public class GenerateProperties {

	private static Properties properties;

	private static Properties properties2;

	static{
		try{
			String filePath = System.getProperty("java.class.path").split(";")[0]+File.separator;
			properties = new Properties();
			properties.load(new InputStreamReader(new FileInputStream(filePath+"generate.properties"),"UTF-8"));
			properties2 = new Properties();
			properties2.load(new InputStreamReader(new FileInputStream(filePath+"application.properties"),"UTF-8"));

		}catch (Exception e) {
		}
	}
	protected static String PROJECT_PATH = System.getProperty("user.dir")+File.separator+"src";
	protected static String ENTITY_PACKAGE_NAME = properties.getProperty("com.boot.jee.orm.entity.package.name");
	protected static String CLASS_NAME = StringUtils.defaultIfEmpty(properties.getProperty("com.boot.jee.orm.entity.class.name"), StringUtils.toCamelCase(properties.getProperty("com.boot.jee.orm.entity.table.name")));	
	protected static String TABLENAME = properties.getProperty("com.boot.jee.orm.entity.table.name").toUpperCase(); 
	protected static String CACHECLASS=StringUtils.defaultIfEmpty(properties.getProperty("com.boot.jee.orm.cache.class"), "com.gitee.hermer.boot.jee.orm.cache.Redis2CacheIBatis");

	protected static String DRIVER = properties2.getProperty("spring.datasource.type");   
	protected static String URLSTR = properties2.getProperty("spring.datasource.url");  
	protected static String USERNAME = properties2.getProperty("spring.datasource.username");  
	protected static String USERPASSWORD = properties2.getProperty("spring.datasource.password");  

	protected static String DAO_PACKAGE_NAME = properties.getProperty("com.boot.jee.orm.dao.package.name");  
	
	protected static String SERVICE_PACKAGE_NAME = properties.getProperty("com.boot.jee.service.package.name");  
	
	protected static String CONTROLLE_PACKAGE_NAME = properties.getProperty("com.boot.jee.controlle.package.name");  
	
	protected static String CLASS_ALIAS = StringUtils.defaultIfEmpty(properties.getProperty("com.boot.jee.orm.entity.class.alias"), CLASS_NAME).toLowerCase();	
	
	

	public static String getCLASS_ALIAS() {
		return CLASS_ALIAS;
	}



	public static String getCONTROLLE_PACKAGE_NAME() {
		return CONTROLLE_PACKAGE_NAME;
	}



	public static void setCONTROLLE_PACKAGE_NAME(String cONTROLLE_PACKAGE_NAME) {
		CONTROLLE_PACKAGE_NAME = cONTROLLE_PACKAGE_NAME;
	}



	public static Properties getProperties() {
		return properties;
	}



	public static Properties getProperties2() {
		return properties2;
	}



	public static String getPROJECT_PATH() {
		return PROJECT_PATH;
	}



	public static String getENTITY_PACKAGE_NAME() {
		return ENTITY_PACKAGE_NAME;
	}



	public static String getCLASS_NAME() {
		return CLASS_NAME;
	}



	public static String getTABLENAME() {
		return TABLENAME;
	}



	public static String getCACHECLASS() {
		return CACHECLASS;
	}



	public static String getDRIVER() {
		return DRIVER;
	}



	public static String getURLSTR() {
		return URLSTR;
	}



	public static String getUSERNAME() {
		return USERNAME;
	}



	public static String getUSERPASSWORD() {
		return USERPASSWORD;
	}



	public static String getDAO_PACKAGE_NAME() {
		return DAO_PACKAGE_NAME;
	}



	public static String getSERVICE_PACKAGE_NAME() {
		return SERVICE_PACKAGE_NAME;
	}



	public static void setProperties(Properties properties) {
		GenerateProperties.properties = properties;
	}



	public static void setProperties2(Properties properties2) {
		GenerateProperties.properties2 = properties2;
	}



	public static void setPROJECT_PATH(String pROJECT_PATH) {
		PROJECT_PATH = pROJECT_PATH;
	}



	public static void setENTITY_PACKAGE_NAME(String eNTITY_PACKAGE_NAME) {
		ENTITY_PACKAGE_NAME = eNTITY_PACKAGE_NAME;
	}



	public static void setCLASS_NAME(String cLASS_NAME) {
		CLASS_NAME = cLASS_NAME;
	}



	public static void setTABLENAME(String tABLENAME) {
		TABLENAME = tABLENAME;
	}



	public static void setCACHECLASS(String cACHECLASS) {
		CACHECLASS = cACHECLASS;
	}



	public static void setDRIVER(String dRIVER) {
		DRIVER = dRIVER;
	}



	public static void setURLSTR(String uRLSTR) {
		URLSTR = uRLSTR;
	}



	public static void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}



	public static void setUSERPASSWORD(String uSERPASSWORD) {
		USERPASSWORD = uSERPASSWORD;
	}



	public static void setDAO_PACKAGE_NAME(String dAO_PACKAGE_NAME) {
		DAO_PACKAGE_NAME = dAO_PACKAGE_NAME;
	}



	public static void setSERVICE_PACKAGE_NAME(String sERVICE_PACKAGE_NAME) {
		SERVICE_PACKAGE_NAME = sERVICE_PACKAGE_NAME;
	}



	public enum ENUM_BASEDAO{
		list,insert,find,update,delete,insertBatch,updateBatch,deleteBatch
	}


}

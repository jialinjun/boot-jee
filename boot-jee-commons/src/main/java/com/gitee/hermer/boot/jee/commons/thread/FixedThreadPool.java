
package com.gitee.hermer.boot.jee.commons.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadPool {

	private static FixedThreadPool instance = null;
	
	private ExecutorService pool;

	private FixedThreadPool() {
		pool = Executors.newFixedThreadPool(36);
    }

	private FixedThreadPool(int threadNum) {
        if (threadNum <= 0) threadNum = 1;
        pool = Executors.newFixedThreadPool(threadNum);
    }

	/**
	 * 获取ThreadPool对象
	 *
	 * @return instance
	 */
	public static FixedThreadPool getInstance() {
		if (instance == null) {
			synchronized (FixedThreadPool.class) {
				if (instance == null)
					instance = new FixedThreadPool();
			}
		}
		return instance;
	}

	/**
	 * 获取ThreadPool对象
	 *
	 * @return instance
	 */
	public static FixedThreadPool getInstance(int threadNum) {
		if (instance == null) {
			synchronized (FixedThreadPool.class) {
				if (instance == null)
					instance = new FixedThreadPool(threadNum);
			}
		}
		return instance;
	}

	/**
	 * 获取ExecutorService
	 * @return
	 */
	public ExecutorService get() {
		return pool;
	}

	/**
	 * 销毁ExecutorService
	 */
	public void destroy() {
		pool.shutdown();
	}

	@Override
	public void finalize() throws Throwable {
		this.destroy();
		super.finalize();
	}

}

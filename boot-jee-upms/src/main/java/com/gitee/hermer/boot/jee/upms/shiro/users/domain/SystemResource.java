package com.gitee.hermer.boot.jee.upms.shiro.users.domain;

import java.io.Serializable;
import com.gitee.hermer.boot.jee.orm.annotation.Id;

public class SystemResource implements Serializable {

	private String resourceUrl;
	private String name;
	private String resourceKey;
	@Id
	private Integer id;
	private Integer resourceParentId;
	private Integer type;
	
	

	public SystemResource(Integer id) {
		super();
		this.id = id;
	}
	
	
	public SystemResource() {
		super();
	}


	public void setResourceUrl(String resourceUrl){
		this.resourceUrl=resourceUrl;
	}
	public String getResourceUrl(){
		return resourceUrl;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}
	public void setResourceKey(String resourceKey){
		this.resourceKey=resourceKey;
	}
	public String getResourceKey(){
		return resourceKey;
	}
	public void setId(Integer id){
		this.id=id;
	}
	public Integer getId(){
		return id;
	}
	public void setResourceParentId(Integer resourceParentId){
		this.resourceParentId=resourceParentId;
	}
	public Integer getResourceParentId(){
		return resourceParentId;
	}
	public void setType(Integer type){
		this.type=type;
	}
	public Integer getType(){
		return type;
	}
}

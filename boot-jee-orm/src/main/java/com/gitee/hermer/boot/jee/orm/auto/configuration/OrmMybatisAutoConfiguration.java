package com.gitee.hermer.boot.jee.orm.auto.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
import com.gitee.hermer.boot.jee.orm.ibatis.plugins.SlowSqlPlugin;
import com.gitee.hermer.boot.jee.orm.properties.OrmIBatisProperties;
import com.gitee.hermer.boot.jee.orm.properties.OrmPageProperties;
import com.gitee.hermer.boot.jee.orm.properties.OrmSlowProperties;
import com.github.pagehelper.PageHelper;



@Configuration    
@EnableConfigurationProperties(value = {OrmPageProperties.class,OrmSlowProperties.class,OrmIBatisProperties.class})   
@ConditionalOnProperty(prefix = "com.boot.jee.orm",value = "factory", havingValue = "mybatis", matchIfMissing = true) 
@AutoConfigureOrder(99)
public class OrmMybatisAutoConfiguration extends UtilsContext {


	@Autowired
	private OrmPageProperties pageProperties;

	@Autowired
	private OrmSlowProperties slowProperties;

	@Autowired
	private OrmIBatisProperties ibatisProperties;

	@Autowired 
	private DataSource dataSource;

	@Bean(name = "sqlSessionFactory")
	@ConditionalOnMissingBean(SqlSessionFactory.class)
	public SqlSessionFactory sqlSessionFactoryBean(Interceptor[] interceptors) {
		try {
			SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
			bean.setDataSource(dataSource);
			PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
			bean.setMapperLocations(resolver.getResources(ibatisProperties.getMapperLocations()));
			bean.setConfigLocation(resolver.getResource(ibatisProperties.getConfigLocations()));
			bean.setPlugins(interceptors);
			return bean.getObject();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}


	@Bean
	@ConditionalOnProperty(prefix = "com.boot.jee.orm.slow.sql", value = "enable",havingValue="true",matchIfMissing = false)    
	@ConditionalOnMissingBean(SlowSqlPlugin.class)
	public SlowSqlPlugin getSlowSqlPlugin(){
		SlowSqlPlugin slowSqlPlugin = new SlowSqlPlugin();
		Properties p = new Properties();
		p.setProperty("executeTimeThreshold", slowProperties.getExecuteTimeThreshold());
		slowSqlPlugin.setProperties(p);
		return slowSqlPlugin;
	}

	@Bean
	@ConditionalOnMissingBean(PageHelper.class)          
	public PageHelper getPageHelper() {
		PageHelper pageHelper = new PageHelper();
		Properties p = new Properties();
		p.setProperty("offsetAsPageNum", pageProperties.getOffsetAsPageNum());
		p.setProperty("rowBoundsWithCount", pageProperties.getRowBoundsWithCount());
		p.setProperty("reasonable", pageProperties.getReasonable());
		p.setProperty("dialect", pageProperties.getDialect());
		pageHelper.setProperties(p);
		return pageHelper;
	}
	
	
}

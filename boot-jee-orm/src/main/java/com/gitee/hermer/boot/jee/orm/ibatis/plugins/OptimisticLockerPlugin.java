package com.gitee.hermer.boot.jee.orm.ibatis.plugins;

import java.sql.Connection;
import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.ibatis.binding.BindingException;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import com.gitee.hermer.boot.jee.commons.bean.utils.BeanCopyUtils;
import com.gitee.hermer.boot.jee.commons.bean.utils.BeanLocator;
import com.gitee.hermer.boot.jee.commons.collection.CollectionUtils;
import com.gitee.hermer.boot.jee.commons.collection.StringCache;
import com.gitee.hermer.boot.jee.commons.exception.ErrorCode;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
import com.gitee.hermer.boot.jee.commons.number.NumberUtils;
import com.gitee.hermer.boot.jee.commons.reflect.ClassUtils;
import com.gitee.hermer.boot.jee.commons.reflect.ReflectUtils;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;
import com.gitee.hermer.boot.jee.commons.verify.Assert;
import com.gitee.hermer.boot.jee.orm.IBaseDao;
import com.gitee.hermer.boot.jee.orm.annotation.Id;
import com.gitee.hermer.boot.jee.orm.annotation.Version;
import com.gitee.hermer.boot.jee.orm.ibatis.locker.OptimisticLockerException;
import com.gitee.hermer.boot.jee.orm.ibatis.locker.cache.Cache;
import com.gitee.hermer.boot.jee.orm.ibatis.locker.cache.Cache.MethodSignature;
import com.gitee.hermer.boot.jee.orm.ibatis.locker.cache.LocalVersionLockerCache;
import com.gitee.hermer.boot.jee.orm.ibatis.locker.cache.VersionLockerCache;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.update.Update;


@Intercepts({ @Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class, Integer.class }) })
public class OptimisticLockerPlugin extends UtilsContext  implements Interceptor{


	private static Version trueLocker;
	private static Version falseLocker;

	static {
		try {
			trueLocker = OptimisticLockerPlugin.class.getDeclaredMethod("versionValueTrue").getAnnotation(Version.class);
			falseLocker = OptimisticLockerPlugin.class.getDeclaredMethod("versionValueFalse").getAnnotation(Version.class);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("The plugin init faild.", e);
		}
	}

	private Properties props = null;
	private VersionLockerCache versionLockerCache = new LocalVersionLockerCache();

	@Version(true)
	private void versionValueTrue() {}

	@Version(false)
	private void versionValueFalse() {}



	public  Object processTarget(Object target) {
		if(Proxy.isProxyClass(target.getClass())) {
			MetaObject mo = SystemMetaObject.forObject(target);
			return processTarget(mo.getValue("h.target"));
		}
		return target;
	}

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		String versionColumn;
		String versionField;
		if(null == props || props.isEmpty()) {
			versionColumn = "version";
			versionField = "version";
		} else {
			versionColumn = props.getProperty("versionColumn", "version");
			versionField = props.getProperty("versionField", "version");
		}
		String interceptMethod = invocation.getMethod().getName();
		if(!"prepare".equals(interceptMethod)) {
			return invocation.proceed();
		}
		StatementHandler handler = (StatementHandler) processTarget(invocation.getTarget());
		MetaObject metaObject = SystemMetaObject.forObject(handler);
		MappedStatement ms = (MappedStatement) metaObject.getValue("delegate.mappedStatement");
		SqlCommandType sqlCmdType = ms.getSqlCommandType();
		if(sqlCmdType != SqlCommandType.UPDATE) {
			return invocation.proceed();
		}
		BoundSql boundSql = (BoundSql) metaObject.getValue("delegate.boundSql");
		Version vl = getVersionLocker(ms, boundSql,versionField);
		if(null != vl && !vl.value()) {
			return invocation.proceed();
		}
		Object params = boundSql.getParameterObject();
		Object target = ClassUtils.newInstance(params.getClass());
		Field field = ClassUtils.getField(params.getClass(), Id.class);
		BeanCopyUtils.copyNotNullPropertiesInclude(params, target, 
				CollectionUtils.newArrayList(field.getName()).toArray(new String[]{}));
		IBaseDao dao = (IBaseDao) BeanLocator.getBean(getMapper(ms));
		target = dao.find(target);
		Object originalVersion = metaObject.getValue("delegate.boundSql.parameterObject."+versionField);
		if(originalVersion == null){
			originalVersion = ReflectUtils.invoke(target, null, ClassUtils.getDeclaredMethod(target.getClass(), 
					StringUtils.genGetter(versionField)));
		}
		if(originalVersion == null || Long.parseLong(originalVersion.toString()) <= 0){
			throw new BindingException("value of version field[" + versionField + "]can not be empty");
		}
		
		
		Object version = ReflectUtils.invoke(target, null, ClassUtils.getDeclaredMethod(target.getClass(), 
				StringUtils.genGetter(versionField)));
		
		Assert.notNullCode(version, ErrorCode.DATA_ERROR,new StringCache("Class:").append(target.getClass()).append("|").append("版本号[").append(versionField).append("]为空！").toString());
		if(NumberUtils.isDigits(version.toString()) && NumberUtils.isDigits(originalVersion.toString()) &&
				NumberUtils.parseNumber(version.toString(), Integer.class) != NumberUtils.parseNumber(originalVersion.toString(), Integer.class)){
			throw new OptimisticLockerException(ErrorCode.SYSTEM_ERROR, new String[]{"Row was updated or deleted by another transaction (or unsaved-value mapping was incorrect): ["+target.getClass()+"#"+
					ReflectUtils.invoke(target, null, ClassUtils.getDeclaredMethod(target.getClass(), 
							StringUtils.genGetter(field.getName())))+"]"});
		}
		
		String originalSql = boundSql.getSql();
		if(isDebugEnabled()) {
			debug("==> originalSql: " + originalSql);
		}
		originalSql = addVersionToSql(originalSql, versionColumn, originalVersion);
		metaObject.setValue("delegate.boundSql.sql", originalSql);
		if(isDebugEnabled()) {
			debug("==> originalSql after add version: " + originalSql);
		}
		return invocation.proceed();
	}

	private String addVersionToSql(String originalSql, String versionColumnName, Object originalVersion){
		try{
			net.sf.jsqlparser.statement.Statement stmt = CCJSqlParserUtil.parse(originalSql);
			if(!(stmt instanceof Update)){
				return originalSql;
			}
			Update update = (Update)stmt;
			if(!contains(update, versionColumnName)){
				buildVersionExpression(update, versionColumnName);
			}
			Expression where = update.getWhere();
			if(where != null){
				AndExpression and = new AndExpression(where, buildVersionEquals(versionColumnName, originalVersion));
				update.setWhere(and);
			}else{
				update.setWhere(buildVersionEquals(versionColumnName, originalVersion));
			}
			return stmt.toString();
		}catch(Exception e){
			e.printStackTrace();
			return originalSql;
		}
	}

	private boolean contains(Update update, String versionColumnName){
		List<Column> columns = update.getColumns();
		for(Column column : columns){
			if(column.getColumnName().equalsIgnoreCase(versionColumnName)){
				return true;
			}
		}
		return false;
	}

	private void buildVersionExpression(Update update,String versionColumnName){

		List<Column> columns = update.getColumns();
		Column versionColumn = new Column();
		versionColumn.setColumnName(versionColumnName);
		columns.add(versionColumn);

		List<Expression> expressions = update.getExpressions();
		Addition add = new Addition();
		add.setLeftExpression(versionColumn);
		add.setRightExpression(new LongValue(1));
		expressions.add(add);
	}

	private Expression buildVersionEquals(String versionColumnName, Object originalVersion){
		EqualsTo equal = new EqualsTo();
		Column column = new Column();
		column.setColumnName(versionColumnName);
		equal.setLeftExpression(column);
		LongValue val = new LongValue(originalVersion.toString());
		equal.setRightExpression(val);
		return equal;
	}

	private Version getVersionLocker(MappedStatement ms, BoundSql boundSql,String versionField) {

		Class<?>[] paramCls = null;
		Object paramObj = boundSql.getParameterObject();

		/******************下面处理参数只能按照下面3个的顺序***********************/
		/******************Process param must order by below ***********************/
		// 1、处理@Param标记的参数
		// 1、Process @Param param
		if(paramObj instanceof MapperMethod.ParamMap<?>) {
			MapperMethod.ParamMap<?> mmp = (MapperMethod.ParamMap<?>) paramObj;
			if(null != mmp && !mmp.isEmpty()) {
				paramCls = new Class<?>[mmp.size() / 2];
				int mmpLen = mmp.size() / 2;
				for(int i=0; i<mmpLen; i++) {
					Object index = mmp.get("param" + (i + 1));
					paramCls[i] = index.getClass();
					if(List.class.isAssignableFrom(paramCls[i])){
						return falseLocker;
					}
				}
			}

			// 2、处理Map类型参数
			// 2、Process Map param
		} else if (paramObj instanceof Map) {//不支持批量
			@SuppressWarnings("rawtypes")
			Map map = (Map)paramObj;
			if(map.get("list") != null || map.get("array") != null){
				return falseLocker;
			}else{
				paramCls = new Class<?>[] {Map.class};
			}
			// 3、处理POJO实体对象类型的参数
			// 3、Process POJO entity param
		} else {
			paramCls = new Class<?>[] {paramObj.getClass()};
		}

		Cache.MethodSignature vm = new MethodSignature(ms.getId(), paramCls);
		Version versionLocker = versionLockerCache.getVersionLocker(vm);
		if(null != versionLocker) {
			return versionLocker;
		}

		Class<?> mapper = getMapper(ms);
		if(mapper != null) {

			for (Class<?> clz : paramCls) { 
				try{
					versionLocker = ClassUtils.getMethodAnnotation(clz, Version.class, StringUtils.genGetter(versionField));
					if(versionLocker != null)
						break;
				}catch (Exception e) {}
			}

			if(null == versionLocker) {
				versionLocker = trueLocker;
			}
			if(!versionLockerCache.containMethodSignature(vm)) {
				versionLockerCache.cacheMethod(vm, versionLocker);
			}
			return versionLocker;
		} else {
			throw new RuntimeException("Config info error, maybe you have not config the Mapper interface");
		}
	}


	private Class<?> getMapper(MappedStatement ms){
		String namespace = getMapperNamespace(ms);
		Collection<Class<?>> mappers = ms.getConfiguration().getMapperRegistry().getMappers();
		for(Class<?> clazz : mappers){
			if(clazz.getName().equals(namespace)){
				return clazz;
			}
		}
		return null;
	}

	private String getMapperNamespace(MappedStatement ms){
		String id = ms.getId();
		int pos = id.lastIndexOf(".");
		return id.substring(0, pos);
	}

	private String getMapperShortId(MappedStatement ms){
		String id = ms.getId();
		int pos = id.lastIndexOf(".");
		return id.substring(pos+1);
	}

	@Override
	public Object plugin(Object target) {
		if (target instanceof StatementHandler || target instanceof ParameterHandler) {
			return Plugin.wrap(target, this);
		} else {
			return target;
		}
	}

	@Override
	public void setProperties(Properties properties) {
		if(null != properties && !properties.isEmpty()) props = properties;
	}


}

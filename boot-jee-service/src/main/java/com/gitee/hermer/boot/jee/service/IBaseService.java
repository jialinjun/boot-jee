package com.gitee.hermer.boot.jee.service;

import java.io.Serializable;
import java.util.List;

import com.github.pagehelper.PageInfo;

public interface IBaseService<T,ID extends Serializable> {
	List<T> list(T t) throws Throwable;
	List<T> list() throws Throwable;
	Integer insert(T t) throws Throwable;
	Integer saveOrUpdate(T t) throws Throwable;
	T find(T t) throws Throwable;
	T find(ID id) throws Throwable;
	Integer update(T t) throws Throwable;
	Integer delete(T t) throws Throwable;
	Integer insertBatch(List<T> list) throws Throwable;
	Integer updateBatch(List<T> list) throws Throwable;
	Integer deleteBatch(List<T> list) throws Throwable;
	PageInfo<T> pageList(T t,Integer pageNum,Integer pageSize) throws Throwable;
}


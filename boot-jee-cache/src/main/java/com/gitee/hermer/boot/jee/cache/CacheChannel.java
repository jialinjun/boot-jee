package com.gitee.hermer.boot.jee.cache;

import java.util.List;


public interface CacheChannel {

	public final static byte LEVEL_1 = 1;
	public final static byte LEVEL_2 = 2;
	
	
	public CacheObject get(String region, Object key);
	
	
	public void set(String region, Object key, Object value);
	
	
	public void set(String region, Object key, Object value, Integer expireInSec);

	
	public void evict(String region, Object key) ;

	
	@SuppressWarnings({ "rawtypes" })
	public void batchEvict(String region, List keys) ;

	
	public void clear(String region) throws CacheException ;
	
	
	@SuppressWarnings("rawtypes")
	public List keys(String region) throws CacheException ;

	
	public void close() ;
}

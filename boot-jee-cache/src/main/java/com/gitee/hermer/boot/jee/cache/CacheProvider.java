package com.gitee.hermer.boot.jee.cache;

import java.util.Properties;


public interface CacheProvider {

	
	public String name();
	
	
	public Cache buildCache(String regionName, boolean autoCreate, CacheExpiredListener listener) throws CacheException;

	
	public void start(Properties props) throws CacheException;

	
	public void stop();
	
}

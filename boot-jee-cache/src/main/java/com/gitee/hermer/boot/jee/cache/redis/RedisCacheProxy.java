package com.gitee.hermer.boot.jee.cache.redis;

import redis.clients.jedis.BinaryJedisPubSub;

import java.io.Closeable;
import java.util.Set;

import com.gitee.hermer.boot.jee.cache.redis.client.RedisClient;
import com.gitee.hermer.boot.jee.cache.redis.support.BlockSupportConfig;
import com.gitee.hermer.boot.jee.cache.redis.support.RedisClientFactoryAdapter;


public class RedisCacheProxy implements Closeable {
    private int timeOutMillis = 60000; 
    private int timeLockMillis = 60000;
    private int stripes = 1024;
    private int timeWaitMillis = 300;
    private boolean block = true;

    private RedisClientFactoryAdapter redisClientFactoryAdapter;


    public RedisCacheProxy(RedisClientFactoryAdapter redisClientFactoryAdapter) {
        this.redisClientFactoryAdapter = redisClientFactoryAdapter;
        if (this.redisClientFactoryAdapter == null) {
            throw new RuntimeException("jedis handler adapter must configuration");
        }
    }

    public RedisCacheProxy(BlockSupportConfig config, RedisClientFactoryAdapter redisClientFactoryAdapter) {
        this(redisClientFactoryAdapter);
        if (config != null) {
            setBlock(config.isBlock());
            setStripes(config.getStripes());
            setTimeLockMillis(config.getTimeLockMillis());
            setTimeWaitMillis(config.getTimeWaitMillis());
            setTimeOutMillis(config.getTimeOutMillis());
        }

    }


    public RedisClient getResource() {
        return this.redisClientFactoryAdapter.getRedisClientFactory().getResource();
    }

    public void returnResource(RedisClient redisClient) {
        this.redisClientFactoryAdapter.getRedisClientFactory().returnResource(redisClient);
    }

    public byte[] hget(byte[] key, byte[] fieldKey) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            return redisClient.hget(key, fieldKey);
        } finally {
            returnResource(redisClient);
        }
    }


    public void hset(byte[] key, byte[] fieldKey, byte[] val) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            redisClient.hset(key, fieldKey, val);
        } finally {
            returnResource(redisClient);
        }
    }


    public void hset(byte[] key, byte[] fieldKey, byte[] val, int expireInSec) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            redisClient.hset(key, fieldKey, val);
            redisClient.expire(key, expireInSec);
        } finally {
            returnResource(redisClient);
        }
    }

    public void hdel(byte[] key, byte[]... fieldKey) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            redisClient.hdel(key, fieldKey);
        } finally {
            returnResource(redisClient);
        }
    }

    public Set<String> hkeys(String key) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            return redisClient.hkeys(key);
        } finally {
            returnResource(redisClient);
        }
    }

    public Set<byte[]> hkeys(byte[] key) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            return redisClient.hkeys(key);
        } finally {
            returnResource(redisClient);
        }
    }

    public void del(String key) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            redisClient.del(key);
        } finally {
            returnResource(redisClient);
        }
    }

    public void del(byte[] key) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            redisClient.del(key);
        } finally {
            returnResource(redisClient);
        }
    }

    public void subscribe(BinaryJedisPubSub binaryJedisPubSub, byte[]... channels) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            redisClient.subscribe(binaryJedisPubSub, channels);
        } finally {
            returnResource(redisClient);
        }
    }

    public void publish(byte[] channel, byte[] message) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            redisClient.publish(channel, message);
        } finally {
            returnResource(redisClient);
        }
    }

    public String set(String key, String value, String nxxx, String expx, long time) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            return redisClient.set(key, value, nxxx, expx, time);
        } finally {
            returnResource(redisClient);
        }

    }

    public String set(byte[] key, byte[] value, byte[] nxxx, byte[] expx, long time) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            return redisClient.set(key, value, nxxx, expx, time);
        } finally {
            returnResource(redisClient);
        }
    }
    
    public String get(String key) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            return redisClient.get(key);
        } finally {
            returnResource(redisClient);
        }
    }

    public Long del(byte[]... keys) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            return redisClient.del(keys);
        } finally {
            returnResource(redisClient);
        }
    }

    public Set<byte[]> keys(byte[] pattern) {
        RedisClient redisClient = null;
        try {
            redisClient = getResource();
            return redisClient.keys(pattern);
        } finally {
            returnResource(redisClient);
        }
    }


    @Override
	public void close() {
        redisClientFactoryAdapter.close();
    }

    public int getTimeOutMillis() {
        return timeOutMillis;
    }

    public void setTimeOutMillis(int timeOutMillis) {
        this.timeOutMillis = timeOutMillis;
    }

    public int getTimeLockMillis() {
        return timeLockMillis;
    }

    public void setTimeLockMillis(int timeLockMillis) {
        this.timeLockMillis = timeLockMillis;
    }

    public int getStripes() {
        return stripes;
    }

    public void setStripes(int stripes) {
        this.stripes = stripes;
    }

    public int getTimeWaitMillis() {
        return timeWaitMillis;
    }

    public void setTimeWaitMillis(int timeWaitMillis) {
        this.timeWaitMillis = timeWaitMillis;
    }

    public boolean isBlock() {
        return block;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }
}
